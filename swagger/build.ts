import fs from "fs";
import { buildSwagger } from "../src/libs/swagger";

fs.writeFileSync(
  __dirname + "/swagger.json",
  JSON.stringify(buildSwagger(), null, 2)
);
