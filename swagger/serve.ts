import express from "express";
import swaggerUi from "swagger-ui-express";
import swaggerDocument from "./swagger.json";

const port = 8123;

async function start() {
  const app = express();
  app.use(
    "/",
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocument, {
      swaggerOptions: {
        authAction: {
          bearerAuth: {
            name: "bearerAuth",
            schema: {
              type: "http",
              in: "header",
              name: "Authorization",
              description: "asd",
            },
            value: "Bearer <JWT>",
          },
        },
      },
    })
  );
  app.listen(port, () => {
    console.log(`Swagger UI listening at http://localhost:${port}`);
  });
}

start();
