const fs = require("fs");

const handlersDir = "./src/handlers/";
const handlersGroups = fs
  .readdirSync(handlersDir, { withFileTypes: true })
  .filter((dirent) => dirent.isDirectory())
  .map((dirent) => dirent.name);

const handlers = handlersGroups.reduce((tot, curr) => {
  const handlers = fs
    .readdirSync(handlersDir + "/" + curr, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => dirent.name);
  return [
    ...tot,
    ...handlers.map((dir) => {
      return {
        directory: dir,
        path: curr + "/" + dir,
        name: curr + " / " + dir,
        group: curr,
      };
    }),
  ];
}, []);

function camelize(str) {
  return str
    .replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
      return index === 0 ? word.toLowerCase() : word.toUpperCase();
    })
    .replace(/\s+/g, "");
}

module.exports = function (plop) {
  // create your generators here
  plop.setGenerator("delete handler", {
    description: "this is a skeleton plopfile",
    prompts: [
      {
        type: "list",
        name: "handler",
        message: "select handler to delete",
        choices: handlers.map((h) => h.name),
      },
    ],
  });
  plop.setGenerator("create handler", {
    description: "this is a skeleton plopfile",
    prompts: [
      {
        type: "list",
        name: "group",
        message: "function group please (to add one create the folder first)",
        choices: handlersGroups,
      },
      {
        type: "input",
        name: "name",
        message: "function name please",
      },
      {
        type: "input",
        name: "route",
        message: "function route please",
        default: (answers) => {
          return camelize(answers.name);
        },
      },
      {
        type: "list",
        name: "method",
        message: "function method please",
        choices: ["get", "post", "patch", "delete", "put"],
      },
      {
        type: "confirm",
        name: "authenticated",
        message: "authenticated?",
      },
    ], // array of inquirer prompts
    actions: [
      {
        type: "add",
        path: "src/handlers/{{ group }}/{{ camelCase name }}/definitions.ts",
        templateFile: "plop/handler/definitions.hb",
      },
      {
        type: "add",
        path: "src/handlers/{{ group }}/{{ camelCase name }}/main.ts",
        templateFile: "plop/handler/main.hb",
      },
      {
        type: "add",
        path: "src/handlers/{{ group }}/{{ camelCase name }}/handler.ts",
        templateFile: "plop/handler/handler.hb",
      },
      {
        type: "append",
        path: "src/handlers.ts",
        pattern: "plop import insert hook",
        template:
          'import {{camelCase name}} from "./handlers/{{group}}/{{camelCase name}}/definitions";',
      },
      {
        type: "append",
        path: "src/handlers.ts",
        pattern: "plop handler insert hook",
        template: "  p({{ camelCase name }}),",
      },
      {
        type: "append",
        path: "src/libs/userGroups.ts",
        pattern: "plop handler permission hook",
        template: '  {{ camelCase name }}: "{{camelCase name}}",',
      },
    ],
  });
};
