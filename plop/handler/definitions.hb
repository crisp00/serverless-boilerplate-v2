import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import { definitions } from "../../../libs/standardDefinitions";
import { UserPermission } from "../../../libs/userGroups";

const inputBody = Joi.object({});

export interface InputBody {

}

const outputBody = Joi.object({
});
export interface OutputBody {
}

export default definitions({
  name: "{{camelCase name}}",
  tags: ["{{group}}"],
  method: HttpMethods.{{ method }},
  path: "{{group}}/{{ route }}",
  filePath: "{{group}}/{{ camelCase name }}",
  authenticated: {{ authenticated }},
  requiredPermissions: [UserPermission.{{ camelCase name }}],
  inputBody,
  outputBody,
});
