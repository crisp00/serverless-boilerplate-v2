import { MainFunction } from "../../../libs/wrapper";
import { InputBody, OutputBody } from "./definitions";

const main: MainFunction<InputBody, OutputBody> = async function main(
  event,
  __,
  { user }
) {
  return {
    statusCode: 200,
    body: {},
  };
};
export default main;
