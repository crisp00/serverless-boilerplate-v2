import * as authorize from "./handlers/auth/authorize/definitions";
// import register from "./handlers/auth/register/definitions";
// import login from "./handlers/auth/login/definitions";
// import refreshTokens from "./handlers/auth/refreshTokens/definitions";
import getProfile from "./handlers/user/getProfile/definitions";
// plop import insert hook
import devGenerateTestData from "./handlers/admin/devGenerateTestData/definitions";
import adminStats from "./handlers/admin/adminStats/definitions";
import inviteUser from "./handlers/user/inviteUser/definitions";
import adminUserAutocomplete from "./handlers/admin/adminUserAutocomplete/definitions";
import adminUserAutocompleteEmpty from "./handlers/admin/adminUserAutocompleteEmpty/definitions";
import adminDeleteFoodEntry from "./handlers/admin/adminDeleteFoodEntry/definitions";
import adminUpsertFoodEntry from "./handlers/admin/adminUpsertFoodEntry/definitions";
import adminGetFoodEntries from "./handlers/admin/adminGetFoodEntries/definitions";
import getFoodEntries from "./handlers/food/getFoodEntries/definitions";
import deleteFoodEntry from "./handlers/food/deleteFoodEntry/definitions";
import addFoodEntry from "./handlers/food/addFoodEntry/definitions";

function p(obj, name?: string) {
  return {
    ...obj,
    name: obj.name || name,
    swagger: obj.swagger(),
    serverless: obj.serverless(),
    requiredPermissions: obj.requiredPermissions || [],
  };
}

const handlers = [
  p(authorize, "authorize"),
  // p(register),
  // p(login),
  // p(refreshTokens),
  p(getProfile),
  // plop handler insert hook
  p(devGenerateTestData),
  p(adminStats),
  p(inviteUser),
  p(adminUserAutocomplete),
  p(adminUserAutocompleteEmpty),
  p(adminDeleteFoodEntry),
  p(adminUpsertFoodEntry),
  p(adminGetFoodEntries),
  p(getFoodEntries),
  p(deleteFoodEntry),
  p(addFoodEntry),
];
export default handlers;
