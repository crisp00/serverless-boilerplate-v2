import moment from "moment";
import { ServerError, ServerErrorCode } from "../../../libs/error";
import { upsertEntry } from "../../../libs/food";
import { MainFunction } from "../../../libs/wrapper";
import { InputBody, OutputBody } from "./definitions";

const main: MainFunction<InputBody, OutputBody> = async function main(
  event,
  __
) {
  const { calories, consumptionTime, id, mealId, name, userId } = event.body;
  const consumptionMoment = moment(consumptionTime);
  if (consumptionMoment.isAfter(moment()))
    throw new ServerError(
      ServerErrorCode.invalid_input,
      "consumptionDate must be a past Date"
    );
  if (!consumptionMoment.isValid())
    throw new ServerError(
      ServerErrorCode.invalid_input,
      "consumptionDate is not a valid Date"
    );

  const finalEntry = await upsertEntry(undefined, {
    calories,
    consumptionTime,
    id,
    mealId,
    name,
    userId,
  });
  return {
    statusCode: 200,
    body: finalEntry,
  };
};
export default main;
