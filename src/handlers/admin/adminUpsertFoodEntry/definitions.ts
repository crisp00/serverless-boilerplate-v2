import { FoodEntry } from "@prisma/client";
import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import MEALS from "../../../libs/food/meals";
import { definitions } from "../../../libs/standardDefinitions";
import { PartialBy } from "../../../libs/tsUtils";
import { UserPermission } from "../../../libs/userGroups";

const inputBody = Joi.object({
  id: Joi.number().integer().strict(),
  name: Joi.string().max(50).required(),
  consumptionTime: Joi.string().isoDate().required(),
  calories: Joi.number().integer().min(0).required().strict().max(5000),
  mealId: Joi.valid(...MEALS.map((m) => m.id))
    .required()
    .strict(),
  userId: Joi.number().integer().required().strict(),
}).required();

export type InputBody =
  | PartialBy<Omit<FoodEntry, "createdAt">, "id">
  | PartialBy<
      Omit<FoodEntry, "createdAt">,
      "calories" | "name" | "consumptionTime" | "mealId" | "userId"
    >;

const outputBody = Joi.object({});
export interface OutputBody {}

export default definitions({
  name: "adminUpsertFoodEntry",
  tags: ["admin"],
  method: HttpMethods.put,
  path: "admin/adminUpsertFoodEntry",
  filePath: "admin/adminUpsertFoodEntry",
  authenticated: true,
  requiredPermissions: [UserPermission.adminUpsertFoodEntry],
  inputBody,
  outputBody,
});
