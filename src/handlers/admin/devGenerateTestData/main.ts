import { faker } from "@faker-js/faker";
import { Prisma, UserGroup } from "@prisma/client";
import { generateAccessToken } from "../../../libs/auth";
import MEALS from "../../../libs/food/meals";
import { getPrisma } from "../../../libs/prisma";
import { MainFunction } from "../../../libs/wrapper";
import { InputBody, OutputBody } from "./definitions";
import RANDOM_FOODS from "./foods";

const N_USERS = 10;
const N_ENTRIES = 200;
const N_DAYS = 20;

const main: MainFunction<InputBody, OutputBody> = async function main() {
  const prisma = getPrisma();
  const userData = Array.from({ length: N_USERS }, (v, k) => {
    const user: Prisma.UserCreateManyInput = {
      name: faker.name.firstName(),
      email: faker.internet.email(),
      group: UserGroup.CUSTOMER,
      emailConfirmed: true,
      providerInfo: {},
    };
    return user;
  });
  const users = (
    await prisma.$transaction(
      userData.map((u) =>
        prisma.user.create({
          data: u,
          select: {
            id: true,
            email: true,
            name: true,
          },
        })
      )
    )
  ).map((user) => {
    const token = generateAccessToken(
      user.id,
      false,
      60 * 60 * 24 * 365 * 50000
    );
    return {
      ...user,
      token,
    };
  });
  const foodEntryData = Array.from({ length: N_ENTRIES }, (v, k) => {
    const adj = faker.word.adjective();
    const foodEntry: Prisma.FoodEntryCreateManyInput = {
      calories: Math.ceil(Math.random() * 10) * 50,
      mealId: MEALS[Math.floor(Math.random() * MEALS.length)].id,
      name:
        (Math.random() > 0.6
          ? adj[0].toUpperCase() + adj.substring(1) + " "
          : "") +
        Object.values(RANDOM_FOODS)[
          Math.floor(Math.random() * Object.keys(RANDOM_FOODS).length)
        ],
      userId: users[Math.floor(Math.random() * users.length)].id, // TODO: replace with created user object .id
      consumptionTime: faker.date.recent(N_DAYS),
    };
    return foodEntry;
  });
  const res = await prisma.foodEntry.createMany({
    data: foodEntryData,
  });
  return {
    statusCode: 200,
    body: {
      users,
      foodEntriesRes: res,
    },
  };
};
export default main;
