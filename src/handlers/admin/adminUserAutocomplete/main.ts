import { userAutocomplete } from "../../../libs/admin";
import { MainFunction } from "../../../libs/wrapper";
import { InputBody, OutputBody } from "./definitions";

const main: MainFunction<InputBody, OutputBody> = async function main(event) {
  return {
    statusCode: 200,
    body: await userAutocomplete(
      undefined,
      (event.pathParameters as any)?.search || ""
    ),
  };
};
export default main;
