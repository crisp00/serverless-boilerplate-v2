import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import { definitions } from "../../../libs/standardDefinitions";
import { UserPermission } from "../../../libs/userGroups";

export interface InputBody {}

const outputBody = Joi.object({});
export interface OutputBody {}

export default definitions({
  name: "adminUserAutocompleteEmpty",
  tags: ["admin"],
  method: HttpMethods.get,
  path: "admin/adminUserAutocomplete",
  filePath: "admin/adminUserAutocomplete",
  authenticated: true,
  requiredPermissions: [UserPermission.adminUserAutocompleteByEmail],
  outputBody,
});
