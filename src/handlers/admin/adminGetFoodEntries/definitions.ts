import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import { definitions } from "../../../libs/standardDefinitions";
import { UserPermission } from "../../../libs/userGroups";

export type InputBody = undefined;

const inputValidator = Joi.object({
  queryStringParameters: Joi.object({
    page: Joi.number().integer().min(0).required(),
    pageSize: Joi.number().integer().min(10).max(100),
  }).required(),
}).unknown();

const outputBody = Joi.object({});
export interface OutputBody {}

export default definitions({
  name: "adminGetFoodEntries",
  tags: ["admin"],
  method: HttpMethods.get,
  path: "admin/adminGetFoodEntries",
  filePath: "admin/adminGetFoodEntries",
  authenticated: true,
  requiredPermissions: [UserPermission.adminGetFoodEntries],
  inputValidator,
  outputBody,
});
