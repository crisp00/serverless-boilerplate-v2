import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import { definitions } from "../../../libs/standardDefinitions";
import { UserPermission } from "../../../libs/userGroups";

const inputValidator = Joi.object({
  pathParameters: Joi.object({
    id: Joi.number().integer().required(),
  }).required(),
}).unknown();

export type InputBody = undefined;

const outputBody = Joi.object({});
export interface OutputBody {}

export default definitions({
  name: "adminDeleteFoodEntry",
  tags: ["admin"],
  method: HttpMethods.delete,
  path: "admin/adminDeleteFoodEntry/{id}",
  filePath: "admin/adminDeleteFoodEntry",
  authenticated: true,
  requiredPermissions: [UserPermission.adminDeleteFoodEntry],
  inputValidator,
  outputBody,
});
