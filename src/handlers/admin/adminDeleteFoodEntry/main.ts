import { ServerError, ServerErrorCode } from "../../../libs/error";
import { deleteEntry } from "../../../libs/food";
import { MainFunction } from "../../../libs/wrapper";
import { InputBody, OutputBody } from "./definitions";

const main: MainFunction<InputBody, OutputBody> = async function main(
  event,
  __
) {
  let id;
  try {
    id = parseInt((event.pathParameters as { id: string }).id, 10);
  } catch (e) {
    throw new ServerError(ServerErrorCode.invalid_input);
  }
  await deleteEntry(undefined, id);
  return {
    statusCode: 204,
  };
};
export default main;
