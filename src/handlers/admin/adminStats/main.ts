import { adminStats } from "../../../libs/admin";
import { MainFunction } from "../../../libs/wrapper";
import { InputBody, OutputBody } from "./definitions";

const main: MainFunction<InputBody, OutputBody> = async function main() {
  const stats = await adminStats();
  return {
    statusCode: 200,
    body: stats,
  };
};
export default main;
