import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import { definitions } from "../../../libs/standardDefinitions";
import { UserPermission } from "../../../libs/userGroups";

export type InputBody = undefined;

const outputBody = Joi.object({});

export interface OutputBody {}

export default definitions({
  name: "adminStats",
  tags: ["admin"],
  method: HttpMethods.get,
  path: "admin/adminStats",
  filePath: "admin/adminStats",
  authenticated: true,
  requiredPermissions: [UserPermission.adminStats],
  outputBody,
});
