import { APIGatewayEvent, Context } from "aws-lambda";
import { createMock } from "ts-auto-mock";
import { prismaMock } from "../../../../tests/singleton";
import { handler } from "./handler";
import { User } from ".prisma/client";
import { consoleOn } from "../../../../tests/setupTests";

const validEmail = "validemail@email.com";
const validPassword = "validPass123@";

async function registerWithValidInput() {
  const input = {
    email: validEmail,
    password: validPassword,
  };
  const user = createMock<User>({
    email: validEmail,
    name: null,
    fiscalCode: null,
  });
  // Return null for no user found with that email
  prismaMock.user.findUnique.mockResolvedValue(null);
  prismaMock.user.create.mockResolvedValue(user);
  const result = await handler(
    createMock<APIGatewayEvent>({ body: JSON.stringify(input) }),
    createMock<Context>(),
    () => null
  );
  return {
    input,
    result,
    user,
  };
}

describe("register", () => {
  it("fails when email is already taken", async () => {
    expect.assertions(2);
    const input = {
      email: validEmail,
      password: validPassword,
    };
    const user = createMock<User>({ email: validEmail });
    prismaMock.user.findUnique.mockResolvedValue(user);
    const res = await handler(
      createMock<APIGatewayEvent>({ body: JSON.stringify(input) }),
      createMock<Context>(),
      () => null
    );
    expect(res.statusCode).toBe(400);
    expect(JSON.parse(res.body).error).toBe("invalid_input");
  });

  it("returns user profile and tokens when  successful", async () => {
    expect.assertions(7);
    consoleOn();
    const { result } = await registerWithValidInput();
    expect(result.statusCode).toStrictEqual(200);
    expect(result.body).toBeTruthy();
    const body = JSON.parse((result.body || "") as string);
    expect(body.user).toBeTruthy();
    expect(body.user.email).toStrictEqual(validEmail);
    expect(body.tokens).toBeTruthy();
    expect(body.tokens.accessToken).toBeTruthy();
    expect(body.tokens.refreshToken).toBeTruthy();
  });
});
