import { generateTokens, registerUserWithPassword } from "../../../libs/auth";
import { mapUserForPublic } from "../../../libs/mappers/user";

export default async function main(event) {
  const { email, password } = event.body;
  const user = await registerUserWithPassword(email, password);
  const tokens = await generateTokens(user);
  return {
    statusCode: 200,
    body: {
      user: mapUserForPublic(user),
      tokens,
    },
  };
}
