import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import { ServerErrorCode } from "../../../libs/errorCodes";
import { TokensJoi, UserJoi } from "../../../libs/types/user";
import { definitions } from "../../../libs/standardDefinitions";

const inputBody = Joi.object().keys({
  email: Joi.string().email().description("Just an email"),
  password: Joi.string(),
});

const outputBody = Joi.object()
  .keys({
    user: UserJoi,
    tokens: TokensJoi,
  })
  .unknown();

export default definitions({
  name: "register",
  summary: "Register a user with email and password",
  // description: "Registers a new user with an email and a password.",
  tags: ["auth"],
  method: HttpMethods.post,
  path: "auth/register",
  inputBody,
  requestBodyDescription: "Email and password",
  responseBodyDescription:
    "Newly created user's profile and access and refresh tokens.",
  outputBody,
  errorResponses: [ServerErrorCode.password_not_secure],
});
