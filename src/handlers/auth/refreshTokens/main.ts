import { getPrisma } from "../../../libs/prisma";
import {
  authenticateToken,
  generateTokens,
  invalidateRefreshToken,
  TokenType,
} from "../../../libs/auth";
import { mapUserForPublic } from "../../../libs/mappers/user";

const prisma = getPrisma();

export default async function main(event) {
  const { refreshToken } = event.body;
  let user = await authenticateToken(refreshToken, TokenType.REFRESH);
  user = await invalidateRefreshToken(user, refreshToken);
  const tokens = await generateTokens(user);
  return {
    statusCode: 200,
    body: {
      user: mapUserForPublic(user),
      tokens,
    },
  };
}
