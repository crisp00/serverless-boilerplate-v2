import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import { TokensJoi, UserJoi } from "../../../libs/types/user";
import { definitions } from "../../../libs/standardDefinitions";

const inputBody = Joi.object().keys({
  refreshToken: Joi.string(),
});

const outputBody = Joi.object().keys({
  user: UserJoi,
  tokens: TokensJoi,
});

export default definitions({
  name: "refreshToken",
  tags: ["auth"],
  method: HttpMethods.post,
  path: "auth/refreshTokens",
  inputBody,
  outputBody,
});
