import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";

export const method = HttpMethods.post;

// TODO: validate the presence of the authorization header
export const inputValidator: Joi.AnySchema = Joi.object()
  .required()
  .unknown(true);

// TODO: this can actually be validated
export const outputValidator: Joi.AnySchema = Joi.any();

export function serverless() {
  return {
    handler: "src/handlers/auth/authorize/handler.handler",
    // type: "TOKEN",
    // identitySource: "method.request.header.Authorization",
  };
}

export function swagger() {
  return [{}, {}];
}
