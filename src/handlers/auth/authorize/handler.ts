import lambdaWrapper from "../../../libs/wrapper";
import main from "./main";
import { inputValidator, outputValidator } from "./definitions";

export const handler = lambdaWrapper({
  inputValidator,
  outputValidator,
  handler: main,
  debug: true,
  noHeaders: true,
  authorizer: true,
});
