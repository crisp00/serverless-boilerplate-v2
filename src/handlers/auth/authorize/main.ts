import {
  authenticateToken,
  userNeedsEmailConfirmation,
} from "../../../libs/auth";
import handlers from "../../../handlers";
import { AuthPolicy } from "../../../libs/aws/authPolicy";
import { groupPermissions, userGroups } from "../../../libs/userGroups";
import { User } from ".prisma/client";
import { ServerError, ServerErrorCode } from "../../../libs/error";

export default async function main(event, _, __, callback) {
  console.log("authorizer running!");
  const authorization = event.authorizationToken;
  const token = authorization
    ? authorization.split(" ")[1] || authorization
    : null;

  let user: User | undefined;
  if (authorization && authorization !== "guest")
    try {
      user = await authenticateToken(token);
    } catch (e) {
      if (
        e instanceof ServerError &&
        e.code === ServerErrorCode.token_expired
      ) {
        await callback("Unauthorized");
        return;
      }
      await callback("Unauthorized");
      return;
    }
  let userPermissions: string[] = [];
  if (user) {
    userPermissions = groupPermissions[user.group];
    if (userNeedsEmailConfirmation(user)) {
      userPermissions = groupPermissions[userGroups.EMAIL_UNCONFIRMED];
    }
  } else {
    userPermissions = groupPermissions[userGroups.GUEST];
  }

  const tmp = event.methodArn.split(":");
  const apiGatewayArnTmp = tmp[5].split("/");
  const awsAccountId = tmp[4];
  const apiOptions = {
    region: tmp[3],
    restApiId: apiGatewayArnTmp[0],
    stage: apiGatewayArnTmp[1],
  };

  const policy = new AuthPolicy(token || "guest", awsAccountId, apiOptions);
  handlers.forEach((handler) => {
    if (!handler.path) return;
    if (
      handler.requiredPermissions.filter((p) => !userPermissions.includes(p))
        .length === 0
    ) {
      policy.allowMethod(
        handler.method.toUpperCase(),
        handler.path.replace(/\{[a-zA-Z0-9]+\}/g, "*")
      );
    } else {
      policy.denyMethod(
        handler.method.toUpperCase(),
        handler.path.replace(/\{[a-zA-Z0-9]+\}/g, "*")
      );
    }
  });

  const authResponse = policy.build();

  authResponse.context = {
    user: JSON.stringify(user),
    permissions: JSON.stringify(userPermissions),
  };

  console.log(JSON.stringify(authResponse, null, 2));

  return { ...authResponse };
}
