import { APIGatewayTokenAuthorizerEvent, Context } from "aws-lambda";
import { createMock } from "ts-auto-mock";
import { generateAccessToken } from "../../../libs/auth";
import { handler } from "./handler";

describe("authorizer", () => {
  it("will call callback with Unauthorized if token is expired", async () => {
    expect.assertions(2);
    const accessToken = generateAccessToken(1, false, -10);
    const event = createMock<APIGatewayTokenAuthorizerEvent>({
      authorizationToken: accessToken,
      methodArn: ":::::",
    });
    const callback = jest.fn();
    const res = await handler(event as any, createMock<Context>(), callback);
    expect(callback).toHaveBeenCalledTimes(1);
    const param = callback.mock.calls[0][0];
    expect(param).toBe("Unauthorized");
  });
});
