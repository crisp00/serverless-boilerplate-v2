import { generateTokens, loginUserWithPassword } from "../../../libs/auth";
import { mapUserForPublic } from "../../../libs/mappers/user";
import { MainFunction } from "../../../libs/wrapper";
import { LoginInputBody, LoginOutputBody } from "./definitions";

const main: MainFunction<LoginInputBody, LoginOutputBody> = async function main(
  event
) {
  const { email, password } = event.body;
  const user = await loginUserWithPassword(email, password);
  const tokens = await generateTokens(user);
  return {
    statusCode: 200,
    body: {
      user: mapUserForPublic(user),
      tokens,
    },
  };
};

export default main;
