import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import {
  TokensJoi,
  TokensResponse,
  UserJoi,
  UserResponse,
} from "../../../libs/types/user";
import { definitions } from "../../../libs/standardDefinitions";

const inputBody = Joi.object().keys({
  email: Joi.string(),
  password: Joi.string(),
});
export interface LoginInputBody {
  email: string;
  password: string;
}

const outputBody = Joi.object().keys({
  user: UserJoi,
  tokens: TokensJoi,
});
export interface LoginOutputBody {
  user: UserResponse;
  tokens: TokensResponse;
}

export default definitions({
  name: "login",
  tags: ["auth"],
  method: HttpMethods.post,
  path: "auth/login",
  inputBody,
  outputBody,
});
