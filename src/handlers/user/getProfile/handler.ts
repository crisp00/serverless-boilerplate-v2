import lambdaWrapper from "../../../libs/wrapper";
import main from "./main";
import definitions from "./definitions";

export const handler = lambdaWrapper({
  inputValidator: definitions.inputValidator,
  outputValidator: definitions.outputValidator,
  handler: main,
  debug: true,
});

// async (event, _context) => {

//   inputSchema.validate({  })
//   return {
//     statusCode: 200,
//     body: JSON.stringify(
//       {
//         message:
//           "Go Serverless Webpack (Typescript) v1.0! Your function executed successfully!",
//         res
//       },
//       null,
//       2
//     ),
//   };
// };
