import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import { UserJoi, UserResponse } from "../../../libs/types/user";
import { definitions } from "../../../libs/standardDefinitions";
import { UserPermission } from "../../../libs/userGroups";

const outputBody = Joi.object({
  user: UserJoi,
});
export interface OutputBody {
  user: UserResponse;
}

export default definitions({
  name: "getProfile",
  tags: ["user"],
  method: HttpMethods.get,
  path: "user/profile",
  filePath: "user/getProfile",
  authenticated: true,
  requiredPermissions: [UserPermission.getProfile],
  outputBody,
});
