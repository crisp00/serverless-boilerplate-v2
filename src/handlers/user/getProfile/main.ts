import { User } from "@prisma/client";
import { getMeals } from "../../../libs/food";
import { mapUserForPublic } from "../../../libs/mappers/user";
import { getPrisma } from "../../../libs/prisma";
import { MainFunction } from "../../../libs/wrapper";
import { OutputBody } from "./definitions";

const prisma = getPrisma();

const main: MainFunction<undefined, OutputBody> = async function main(
  _,
  __,
  { user }
) {
  const userFromDb = (await prisma.user.findUnique({
    where: {
      id: user.id,
    },
  })) as User;
  const meals = await getMeals();
  return {
    statusCode: 200,
    body: {
      user: { ...mapUserForPublic(userFromDb) },
      meals,
    },
  };
};
export default main;
