import lambdaWrapper from "../../../libs/wrapper";
import main from "./main";
import definitions from "./definitions";

export const handler = lambdaWrapper({
  inputValidator: definitions.inputValidator,
  outputValidator: definitions.outputValidator,
  handler: main,
  debug: true,
});
