import { registerUserWithoutProvider } from "../../../libs/auth";
import { MainFunction } from "../../../libs/wrapper";
import { InputBody, OutputBody } from "./definitions";

const main: MainFunction<InputBody, OutputBody> = async function main(event) {
  const { email, name } = event.body;
  return {
    statusCode: 200,
    body: await registerUserWithoutProvider(undefined, email, name),
  };
};
export default main;
