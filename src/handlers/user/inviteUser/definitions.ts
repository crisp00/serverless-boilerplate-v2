import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import { definitions } from "../../../libs/standardDefinitions";
import { UserPermission } from "../../../libs/userGroups";

const inputBody = Joi.object({
  email: Joi.string().email().max(320).required(),
  name: Joi.string().max(50).required(),
});

export interface InputBody {
  email: string;
  name: string;
}

const outputBody = Joi.object({});
export interface OutputBody {}

export default definitions({
  name: "inviteUser",
  tags: ["user"],
  method: HttpMethods.post,
  path: "user/inviteUser",
  filePath: "user/inviteUser",
  authenticated: true,
  requiredPermissions: [UserPermission.inviteUser],
  inputBody,
  outputBody,
});
