import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import { definitions } from "../../../libs/standardDefinitions";
import { UserPermission } from "../../../libs/userGroups";

const inputValidator = Joi.object({
  pathParameters: Joi.object({
    id: Joi.number().integer().required(),
  }),
}).unknown();

export type InputBody = undefined;

export type OutputBody = undefined;

export default definitions({
  name: "deleteFoodEntry",
  tags: ["food"],
  method: HttpMethods.delete,
  path: "food/deleteFoodEntry/{id}",
  filePath: "food/deleteFoodEntry",
  authenticated: true,
  inputValidator,
  requiredPermissions: [UserPermission.deleteFoodEntry],
});
