import { ServerError, ServerErrorCode } from "../../../libs/error";
import { deleteEntryForUser } from "../../../libs/food";
import { MainFunction } from "../../../libs/wrapper";
import { InputBody, OutputBody } from "./definitions";

const main: MainFunction<InputBody, OutputBody> = async function main(
  event,
  __,
  { user }
) {
  const { id } = event.pathParameters as { id: string };
  let entryId: number;
  try {
    entryId = parseInt(id, 10);
    if (!(entryId < 0 || entryId >= 0)) throw new Error();
  } catch (e) {
    throw new ServerError(ServerErrorCode.invalid_input, "invalid id");
  }
  await deleteEntryForUser(undefined, entryId, user.id);
  return {
    statusCode: 204,
  };
};
export default main;
