import { FoodEntry } from "@prisma/client";
import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import MEALS from "../../../libs/food/meals";
import { definitions } from "../../../libs/standardDefinitions";
import { UserPermission } from "../../../libs/userGroups";

const inputBody = Joi.object({
  name: Joi.string().max(50).required(),
  consumptionTime: Joi.string().isoDate().required(),
  calories: Joi.number().strict().integer().required().min(0).max(5000),
  mealId: Joi.valid(...MEALS.map((m) => m.id))
    .required()
    .strict(),
}).required();
export type InputBody = Pick<
  FoodEntry,
  "name" | "consumptionTime" | "calories" | "mealId"
>;

const outputBody = Joi.object({});
export interface OutputBody {}

export default definitions({
  name: "addFoodEntry",
  tags: ["food"],
  method: HttpMethods.post,
  path: "food/addFoodEntry",
  filePath: "food/addFoodEntry",
  authenticated: true,
  requiredPermissions: [UserPermission.addFoodEntry],
  inputBody,
  outputBody,
});
