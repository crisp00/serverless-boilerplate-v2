import moment from "moment";
import { ServerError, ServerErrorCode } from "../../../libs/error";
import { upsertEntryForUser } from "../../../libs/food";
import { MainFunction } from "../../../libs/wrapper";
import { InputBody, OutputBody } from "./definitions";

const main: MainFunction<InputBody, OutputBody> = async function main(
  event,
  __,
  { user }
) {
  let { name, consumptionTime, calories, mealId } = event.body;
  let consumptionMoment = moment(consumptionTime);
  if (consumptionMoment.isAfter(moment()))
    throw new ServerError(
      ServerErrorCode.invalid_input,
      "Cannot add an entry in the future."
    );
  if (!consumptionMoment.isValid())
    throw new ServerError(
      ServerErrorCode.invalid_input,
      "consumptionDate is not a valid Date"
    );
  const createdEntry = await upsertEntryForUser(
    undefined,
    { name, consumptionTime: consumptionMoment.toDate(), calories, mealId },
    user.id
  );
  return {
    statusCode: 200,
    body: createdEntry,
  };
};
export default main;
