import { ServerError, ServerErrorCode } from "../../../libs/error";
import { getFoodEntriesForUser } from "../../../libs/food";
import { MainFunction } from "../../../libs/wrapper";
import { InputBody, OutputBody } from "./definitions";

const main: MainFunction<InputBody, OutputBody> = async function main(
  event,
  __,
  { user }
) {
  const query = event.queryStringParameters as {
    page: string;
    pageSize?: string;
  };
  let page, pageSize;
  page = parseInt(query.page, 10);
  if (query.pageSize) {
    pageSize = parseInt(query.pageSize, 10);
  }
  const res = await getFoodEntriesForUser(undefined, user.id, page, pageSize);
  return {
    statusCode: 200,
    body: res,
  };
};
export default main;
