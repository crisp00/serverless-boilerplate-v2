import * as Joi from "joi";
import { HttpMethods } from "../../../libs/constants";
import { definitions } from "../../../libs/standardDefinitions";
import { UserPermission } from "../../../libs/userGroups";

const inputValidator = Joi.object({
  queryStringParameters: Joi.object({
    page: Joi.number().integer().min(0).required(),
    pageSize: Joi.number().integer().min(10).max(100),
  }).required(),
}).unknown();

export type InputBody = undefined;

const outputBody = Joi.object({});
export interface OutputBody {}

export default definitions({
  name: "getFoodEntries",
  tags: ["food"],
  method: HttpMethods.get,
  path: "food/getFoodEntries",
  filePath: "food/getFoodEntries",
  authenticated: true,
  requiredPermissions: [UserPermission.getFoodEntries],
  inputValidator,
  outputBody,
});
