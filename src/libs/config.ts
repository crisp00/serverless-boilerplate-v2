export default {
  emailConfirmationCodeValiditySeconds: 60 * 60,
  emailConfirmationResendMinimumTimeElapsedSeconds: 5 * 60,
  emailConfirmationMaxResends: 10,
  passwordResetCodeValiditySeconds: 10 * 60,
  passwordResetResendMinimumTimeElapsedSeconds: 5 * 60,
  internalAuthToken: process.env.INTERNAL_AUTH_TOKEN,
  serverBaseUrl: process.env.SERVER_URL,
  shortUrlBase: process.env.SHORT_URL_BASE,
  stage: process.env.SLS_STAGE,
  envShortName: process.env.ENV_SHORT_NAME,
  projectName: "backend",
  webapp: {
    baseUrl: process.env.APP_URL,
    passwordResetUrlTemplate: (code: string, email: string): string =>
      `${process.env.APP_URL}/recover-password/change-password?code=${code}&email=${email}`,
  },
  jwt: {
    secret: process.env.JWT_SECRET || "dev-secret",
    accessTokenDurationSeconds: 4 * 60 * 60, // 4 hours
    refreshTokenDurationSeconds: 6 * 30 * 24 * 60 * 60, // 6 months
  },
  google: {
    clientId: process.env.GOOGLE_AUTH_CLIENT_ID,
  },
  facebook: {
    apiBase: "https://graph.facebook.com",
    clientId: process.env.FACEBOOK_AUTH_CLIENT_ID,
    clientSecret: process.env.FACEBOOK_AUTH_CLIENT_SECRET,
  },
};
