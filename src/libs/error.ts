import { ServerErrorCode, SERVER_ERROR_MAP } from "./errorCodes";

export class ServerError extends Error {
  code: ServerErrorCode;

  constructor(code?: ServerErrorCode, message?: string) {
    super(
      message ||
        SERVER_ERROR_MAP[code || ServerErrorCode.server_error].description
    );
    this.code = code === undefined ? ServerErrorCode.server_error : code;
  }

  statusCode(): number {
    return SERVER_ERROR_MAP[this.code].statusCode;
  }

  errorName(): string {
    return Object.keys(ServerErrorCode)[
      Object.values(ServerErrorCode).indexOf(this.code)
    ];
  }
}

export { ServerErrorCode };
