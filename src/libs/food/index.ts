import { FoodEntry } from "@prisma/client";
import moment from "moment";
import { ServerError, ServerErrorCode } from "../error";
import { getPrisma } from "../prisma";
import { Either, PartialBy, XOR } from "../tsUtils";
import MEALS from "./meals";

export async function getMeals() {
  return MEALS;
}

export async function upsertEntryForUser(
  prisma = getPrisma(),
  entry: PartialBy<Omit<FoodEntry, "createdAt">, "id" | "userId">,
  userId: number
) {
  const meal = MEALS.find((m) => m.id === entry.mealId);
  if (!meal) throw new ServerError(ServerErrorCode.not_found, "Meal not found");
  let foodEntry;
  if (typeof entry.id === "number") {
    const exists = await prisma.foodEntry.findMany({
      where: {
        id: entry.id,
        userId,
      },
    });
    if (!exists.length)
      throw new ServerError(ServerErrorCode.not_found, "FoodEntry not found");
    foodEntry = await prisma.foodEntry.update({
      where: { id: entry.id },
      data: entry,
    });
  } else {
    const exisingEntriesInMealInDay = await prisma.foodEntry.findMany({
      where: {
        mealId: meal.id,
        userId,
        consumptionTime: {
          gte: moment(entry.consumptionTime).startOf("day").toDate(),
          lte: moment(entry.consumptionTime).endOf("day").toDate(),
        },
      },
    });
    if (exisingEntriesInMealInDay.length >= meal.maxDailyEntries)
      throw new ServerError(ServerErrorCode.max_entries_in_meal_for_day);
    foodEntry = await prisma.foodEntry.create({
      data: { ...entry, userId },
    });
  }
  return foodEntry;
}

export async function upsertEntry(
  prisma = getPrisma(),
  entry: PartialBy<
    Omit<FoodEntry, "createdAt">,
    "id" | "calories" | "name" | "consumptionTime" | "mealId" | "userId"
  >
) {
  let meal;
  if (typeof entry.mealId === "number") {
    meal = MEALS.find((m) => m.id === entry.mealId);
    if (!meal)
      throw new ServerError(ServerErrorCode.not_found, "Meal not found");
  }
  let foodEntry;
  if (typeof entry.id === "number") {
    try {
      foodEntry = await prisma.foodEntry.update({
        where: { id: entry.id },
        data: entry,
      });
    } catch (e) {
      if (e.code === "P2025")
        throw new ServerError(ServerErrorCode.not_found, "FoodEntry not found");
      if (e.code === "P2003")
        throw new ServerError(ServerErrorCode.not_found, "User not found");
      console.error(e);
      throw e;
    }
  } else {
    if (
      typeof entry.calories === "undefined" ||
      !meal ||
      typeof entry.userId === "undefined" ||
      !entry.name
    )
      throw new ServerError(ServerErrorCode.invalid_input);
    const exisingEntriesInMealInDay = await prisma.foodEntry.findMany({
      where: {
        mealId: meal.id,
        userId: entry.userId,
        consumptionTime: {
          gte: moment(entry.consumptionTime).startOf("day").toDate(),
          lte: moment(entry.consumptionTime).endOf("day").toDate(),
        },
      },
    });
    if (exisingEntriesInMealInDay.length >= meal.maxDailyEntries)
      throw new ServerError(ServerErrorCode.max_entries_in_meal_for_day);
    try {
      foodEntry = await prisma.foodEntry.create({
        data: {
          calories: entry.calories,
          mealId: meal.id,
          name: entry.name,
          consumptionTime: entry.consumptionTime || undefined,
          userId: entry.userId,
        },
      });
    } catch (e) {
      if (e.code === "P2003")
        throw new ServerError(ServerErrorCode.not_found, "User not found");
      console.error(e);
      throw e;
    }
  }
  return foodEntry;
}

export async function deleteEntryForUser(
  prisma = getPrisma(),
  entryId: number,
  userId: number
) {
  const res = await prisma.foodEntry.deleteMany({
    where: {
      id: entryId,
      userId,
    },
  });
  if (res.count === 0)
    throw new ServerError(ServerErrorCode.not_found, "FoodEntry not found");
}

export async function deleteEntry(prisma = getPrisma(), entryId: number) {
  try {
    return await prisma.foodEntry.delete({
      where: {
        id: entryId,
      },
    });
  } catch (e) {
    if (e.code === "P2025")
      throw new ServerError(ServerErrorCode.not_found, "FoodEntry not found");
    console.error(e);
    throw e;
  }
}

export async function getFoodEntriesForUser(
  prisma = getPrisma(),
  userId: number,
  page = 0,
  pageSize = 50
) {
  return prisma.foodEntry.findMany({
    where: {
      userId,
    },
    orderBy: {
      consumptionTime: "desc",
    },
    skip: page * pageSize,
    take: pageSize,
  });
}

export async function getFoodEntries(
  prisma = getPrisma(),
  page = 0,
  pageSize = 50
) {
  const res = await prisma.$transaction([
    prisma.foodEntry.count(),
    prisma.foodEntry.findMany({
      orderBy: {
        id: "desc",
      },
      skip: page * pageSize,
      take: pageSize,
      include: {
        user: {
          select: {
            id: true,
            name: true,
            email: true,
          },
        },
      },
    }),
  ]);
  return {
    total: res[0],
    pageSize,
    pages: Math.ceil(res[0] / pageSize),
    page,
    data: res[1],
  };
}
