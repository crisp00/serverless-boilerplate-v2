function meal(id: number, name: string, maxDailyEntries: number) {
  return {
    id,
    name,
    maxDailyEntries,
  };
}

const MEALS = [
  meal(0, "Breakfast", 3),
  meal(1, "Lunch", 5),
  meal(2, "Dinner", 2),
];

export default MEALS;
