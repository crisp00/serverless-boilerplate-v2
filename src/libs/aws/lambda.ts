import aws from "aws-sdk";
import config from "../config";

export async function invokeLambda(functionName, payload) {
  var lambda = new aws.Lambda();
  console.log(JSON.stringify({ body: payload }));
  var opts = {
    FunctionName: `${config.projectName}-${config.stage}-${functionName}`,
    Payload: JSON.stringify({ body: JSON.stringify(payload) }),
    InvocationType: "Event",
    LogType: "Tail",
  };
  console.log(opts);
  await new Promise((resolve) => {
    lambda.invoke(opts, (err, res) => {
      console.log(err, res);
      resolve({ err, res });
    });
  });
}
