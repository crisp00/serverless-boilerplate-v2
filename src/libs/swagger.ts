import handlers from "../handlers";

const paths = {};
let components = {};

handlers.forEach((handler) => {
  let path = handler.path;
  if (!path) return;
  if (!paths[`/${path}`]) {
    paths[`/${path}`] = {
      [handler.method]: handler.swagger[0],
    };
  } else {
    paths[`/${path}`][handler.method] = handler.swagger[0];
  }
  if (handler.swagger[1]) components = { ...components, ...handler.swagger[1] };
});

export function buildSwagger() {
  return {
    openapi: "3.0.0",
    info: {
      title: "boilerplate",
      description:
        "Optional multiline or single-line description in [CommonMark](http://commonmark.org/help/) or HTML.",
      version: "0.1.9",
    },
    components: {
      securitySchemes: {
        bearerAuth: {
          type: "http",
          scheme: "bearer",
          bearerFormat: "JWT",
        },
      },
      schemas: {
        ...components,
      },
    },
    servers: [
      {
        url: "http://localhost:8001/dev",
        description:
          "Optional server description, e.g. Main (production) server",
      },
    ],
    paths,
  };
}
