import moment from "moment";
import { User } from ".prisma/client";
import config from "./config";
import { ServerError } from "./error";
import { ServerErrorCode } from "./errorCodes";
import { getPrisma } from "./prisma";

const randomCodeAlphabet = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
const codeValiditySeconds = config.emailConfirmationCodeValiditySeconds;

const prisma = getPrisma();

export function generateRandomCode(length: number): string {
  let code = "";
  for (let i = 0; i < length; i += 1) {
    code +=
      randomCodeAlphabet[Math.floor(Math.random() * randomCodeAlphabet.length)];
  }
  return code;
}

export function ensureCanResendEmailConfirmationCode(user: User): void {
  if (user.emailConfirmed)
    throw new ServerError(
      ServerErrorCode.invalid_input,
      "Email already confirmed"
    );
  // if (user.emailConfirmationResendsCount > config.emailConfirmationMaxResends)
  //   throw new ServerError(
  //     ServerErrorCode.invalid_input,
  //     "Maximum attempts reached"
  //   );
  if (user.emailConfirmationResendsCount > 0) {
    if (
      moment(user.emailConfirmationCodeGenerationTime)
        .add(config.emailConfirmationResendMinimumTimeElapsedSeconds, "s")
        .isAfter(moment())
    )
      throw new ServerError(
        ServerErrorCode.invalid_input,
        "You need to wait some time before you can request a new code"
      );
  }
}

export async function sendNewEmailConfirmationCode(
  userId: number,
  incrementCount = true
): Promise<void> {
  const code = generateRandomCode(5);
  const user = await prisma.user.update({
    where: {
      id: userId,
    },
    data: {
      emailConfirmationCode: code,
      emailConfirmationCodeGenerationTime: new Date(),
      ...(incrementCount
        ? {
            emailConfirmationResendsCount: {
              increment: 1,
            },
          }
        : {}),
    },
  });
}

export async function confirmEmailWithCode(
  user: User,
  code: string
): Promise<User> {
  if (user.emailConfirmed)
    throw new ServerError(
      ServerErrorCode.invalid_input,
      "The user's email has already been confirmed"
    );
  if (
    user.emailConfirmationCode !== code ||
    moment(user.emailConfirmationCodeGenerationTime)
      .add(codeValiditySeconds, "s")
      .isBefore(moment())
  )
    throw new ServerError(
      ServerErrorCode.invalid_input,
      "The code is not right"
    );
  const updatedUser = await prisma.user.update({
    where: {
      id: user.id,
    },
    data: {
      emailConfirmed: true,
    },
  });
  return updatedUser;
}
