import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  Callback,
  Context,
} from "aws-lambda";
import * as Joi from "joi";
import { inspect } from "util";
import warmer from "lambda-warmer";
import { User } from ".prisma/client";
import { ServerErrorCode } from "./errorCodes";
import { ServerError } from "./error";
import { getPrisma } from "./prisma";

const VALIDATE_OUTPUT: boolean = false;
export interface MainFunctionResult<BodyType> {
  statusCode: number;
  body?: BodyType;
  headers?: Record<string, unknown>;
}

export interface MainEventData<BodyType>
  extends Omit<APIGatewayProxyEvent, "body"> {
  body: BodyType;
}

export interface ErrorBodyType {
  error?: string;
  message?: string;
}

export type MainFunction<InputBodyType, OutputBodyType> = (
  eventData: MainEventData<InputBodyType>,
  context: Context,
  authContext: {
    user: User;
    permissions: string[];
  },
  callback?: Callback
) => Promise<MainFunctionResult<OutputBodyType | ErrorBodyType>>;

function validateOrThrow(validator: Joi.AnySchema, data, errorCode) {
  const validationResult = validator.validate(data);
  if (validationResult.error) {
    console.log(
      "Failed IO validation for data: ",
      JSON.stringify(data, null, 2)
    );
    throw new ServerError(errorCode, validationResult.error.message);
  }
  return validationResult;
}

export type LambdaHandler = (
  event: APIGatewayProxyEvent,
  context: Context,
  callback: Callback<APIGatewayProxyResult>
) => Promise<APIGatewayProxyResult>;

const prisma = getPrisma();

export default function lambdaWrapper<InputBodyType, OutputBodyType>({
  inputValidator,
  outputValidator,
  handler,
  debug = false,
  noHeaders = false,
  authorizer = false,
}: {
  inputValidator: Joi.AnySchema;
  outputValidator: Joi.AnySchema;
  handler: MainFunction<InputBodyType, OutputBodyType>;
  debug?: boolean;
  noHeaders?: boolean;
  authorizer?: boolean;
}): LambdaHandler {
  const func: (
    event: APIGatewayProxyEvent,
    context: Context,
    callback: Callback<APIGatewayProxyResult>
  ) => Promise<APIGatewayProxyResult> = async (event_, context, callback) => {
    console.log("function called and logging");
    if (await warmer(event_)) {
      console.log("warmed");
      return "warmed";
    }

    function getHeaders(headers?) {
      let acao = "example.com";
      const requestHeaders = event.headers || {};
      // TODO: fix this
      // if (process.env.NODE_ENV === "development") {
      acao = requestHeaders.origin || requestHeaders.Origin || "example.com";
      // }
      return noHeaders
        ? headers
        : {
            ...(headers || {}),
            "Access-Control-Allow-Origin": acao,
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Methods": "*",
          };
    }

    let done = false;
    const wrappedCallback = async (...args) => {
      done = true;
      // TODO: remove in production
      // This is useful in development because serverless-offline doesn't simulate lambda's context reuse and leaves the connection hanging around
      await prisma.$disconnect();
      callback(...args);
    };
    const body: InputBodyType = event_.body
      ? JSON.parse(event_.body)
      : undefined;
    const event: MainEventData<InputBodyType> = {
      ...event_,
      body,
    };
    let result: MainFunctionResult<OutputBodyType | ErrorBodyType>;
    try {
      if (debug) {
        // eslint-disable-next-line no-console
        console.log("debug event", event);
      }

      validateOrThrow(inputValidator, event, ServerErrorCode.invalid_input);
      let user;
      let permissions;
      if (event.requestContext && event.requestContext.authorizer) {
        user = JSON.parse(
          event.requestContext.authorizer.user || "null"
        ) as User;
        permissions = JSON.parse(
          event.requestContext.authorizer.permissions || "[]"
        );
      }
      result = await new Promise((resolve, reject) => {
        handler(
          event,
          context,
          { user, permissions },
          authorizer ? wrappedCallback : undefined
        )
          .then((result) => {
            if (done) return resolve();
            if (VALIDATE_OUTPUT)
              validateOrThrow(
                outputValidator,
                result,
                ServerErrorCode.server_error
              );
            resolve(result);
          })
          .catch((error) => {
            reject(error);
          });
      });
      if (done) return;
    } catch (e) {
      console.error(e);
      if (e instanceof ServerError) {
        console.log("ERROR", e.statusCode(), e.code);
        result = {
          statusCode: e.statusCode(),
          body: {
            error: e.errorName(),
            message: e.message,
          },
          headers: getHeaders(),
        };
      } else {
        console.log("UNHANDLED ERROR", e.statusCode(), e.code);
        result = {
          statusCode: 500,
        };
      }
    }
    const b = result.body ? JSON.stringify(result.body) : "";
    if (debug) {
      // eslint-disable-next-line no-console
      console.log("debug return value", {
        ...result,
        body: authorizer ? undefined : b,
        headers: getHeaders(result.headers),
      });
    }
    // TODO: remove in production
    // This is useful in development because serverless-offline doesn't simulate lambda's context reuse and leaves the connection hanging around
    await prisma.$disconnect();
    return {
      ...result,
      body: authorizer ? undefined : b,
      headers: getHeaders(result.headers),
    };
  };
  return func;
}
