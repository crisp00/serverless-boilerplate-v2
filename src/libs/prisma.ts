import { PrismaClient } from "@prisma/client";

let prisma: PrismaClient;

let contextUsageCount = 0;

export function getPrisma() {
  console.log("Context used times: " + contextUsageCount);
  contextUsageCount++;
  if (!prisma) {
    prisma = new PrismaClient({
      log: ["query", "info", "warn", "error"],
    });
    prisma.$use(async (params, next) => {
      const before = Date.now();

      const result = await next(params);

      const after = Date.now();
      console.log(
        `Query ${params.model}.${params.action} took ${after - before}ms`
      );

      return result;
    });
  }
  return prisma;
}
