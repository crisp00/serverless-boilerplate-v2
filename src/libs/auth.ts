import jwt from "jsonwebtoken";
import moment from "moment";
import { AuthProvider, Prisma, User } from ".prisma/client";
import config from "./config";
import { ServerError, ServerErrorCode } from "./error";
import { getPrisma } from "./prisma";
import authenticationProviders, {
  getProvider,
} from "./authenticationProviders";
import { createUser } from "./user";
import { generateRandomCode, NUMBERS_LETTERS } from "./random";

const prisma = getPrisma();

export enum TokenType {
  ACCESS = "access",
  REFRESH = "refresh",
  RESET_PASSWORD = "resetPassword",
}

export interface TokenPayload {
  sub: number;
  iat: number;
  nec?: boolean;
  exp: number;
  type: TokenType;
}

export function userNeedsEmailConfirmation(user: User): boolean {
  let needsEmailConfirmation = true;
  if (
    user.providers.filter(
      (p) => getProvider(p).requiresEmailConfirmation === false
    ).length > 0
  )
    needsEmailConfirmation = false;
  if (user.emailConfirmed) needsEmailConfirmation = false;
  return needsEmailConfirmation;
}

export async function authenticateToken(
  token: string,
  expectedType: TokenType = TokenType.ACCESS,
  secret: string = config.jwt.secret
): Promise<User> {
  let payload;
  try {
    payload = jwt.verify(token, secret);
  } catch (e) {
    if (e instanceof jwt.TokenExpiredError) {
      throw new ServerError(
        ServerErrorCode.token_expired,
        "This token is expired"
      );
    }
    throw e;
  }
  if (payload.type !== expectedType)
    throw new ServerError(
      ServerErrorCode.unauthorized,
      "That's not the right type of token"
    );
  console.log("QUERYING USER FROM DB");
  const user = await prisma.user.findUnique({
    where: {
      id: payload.sub,
    },
  });
  if (!user)
    throw new ServerError(ServerErrorCode.unauthorized, "User not found");
  if (expectedType === TokenType.REFRESH && !user.refreshTokens.includes(token))
    throw new ServerError(
      ServerErrorCode.unauthorized,
      "Unrecognized refresh token"
    );
  return user;
}

function generateToken(
  userId,
  expires,
  type,
  needsEmailConfirmation,
  secret = config.jwt.secret
): string {
  const payload: TokenPayload = {
    sub: userId,
    nec: needsEmailConfirmation,
    iat: moment().unix(),
    exp: expires.unix(),
    type,
  };
  return jwt.sign(payload, secret);
}

export function generateAccessToken(
  userId: number,
  needsEmailConfirmation: boolean,
  duration: number = config.jwt.accessTokenDurationSeconds
): string {
  const accessTokenExpires = moment().add(duration, "seconds");
  return generateToken(
    userId,
    accessTokenExpires,
    TokenType.ACCESS,
    needsEmailConfirmation
  );
}

export async function generateRefreshToken(
  user: User,
  needsEmailConfirmation: boolean
): Promise<string> {
  const refreshTokenExpires = moment().add(
    config.jwt.refreshTokenDurationSeconds,
    "seconds"
  );

  const token = generateToken(
    user.id,
    refreshTokenExpires,
    TokenType.REFRESH,
    needsEmailConfirmation
  );
  await prisma.user.update({
    where: {
      id: user.id,
    },
    data: {
      refreshTokens: [...user.refreshTokens, token],
    },
  });
  return token;
}

export async function invalidateRefreshToken(
  user: User,
  token: string
): Promise<User> {
  return prisma.user.update({
    where: {
      id: user.id,
    },
    data: {
      refreshTokens: user.refreshTokens.filter((t) => t !== token),
    },
  });
}

export async function generateTokens(user: User): Promise<{
  accessToken: string;
  refreshToken: string;
}> {
  const needsEmailConfirmation = userNeedsEmailConfirmation(user);
  return {
    accessToken: generateAccessToken(user.id, needsEmailConfirmation),
    refreshToken: await generateRefreshToken(user, needsEmailConfirmation),
  };
}

export async function registerUserWithoutProvider(
  prisma = getPrisma(),
  email: string,
  name?: string
) {
  let user: Partial<User> | null = await prisma.user.findUnique({
    where: {
      email,
    },
  });

  if (user)
    throw new ServerError(
      ServerErrorCode.invalid_input,
      "there already is a user with that email"
    );
  const password = generateRandomCode(
    10 + Math.floor(Math.random() * 5),
    NUMBERS_LETTERS
  );
  const newUser = {
    email,
    name,
    providers: [AuthProvider.PASSWORD],
    providerInfo: {
      password:
        await authenticationProviders.password.getProviderInfoForNewUser(
          password
        ),
    },
    emailConfirmed: true,
  };
  user = await prisma.user.create({
    data: newUser,
    select: {
      id: true,
      email: true,
      name: true,
    },
  });
  return {
    user,
    accessToken: generateAccessToken(
      (user as User).id,
      false,
      60 * 60 * 24 * 365 * 50000 // token valid for 50k years
    ),
    password,
  };
}

export async function registerUserWithPassword(
  email: string,
  password: string
): Promise<User> {
  if (!password.match(/^[\w\.\,\+@#/\\]{6,256}$/))
    throw new ServerError(
      ServerErrorCode.invalid_input,
      "The password doesn't meet the minimum complexity criteria"
    );

  let user = await prisma.user.findUnique({
    where: {
      email,
    },
  });

  if (user)
    throw new ServerError(
      ServerErrorCode.invalid_input,
      "there already is a user with that email"
    );
  const newUser = {
    email,
    providers: [AuthProvider.PASSWORD],
    providerInfo: {
      password:
        await authenticationProviders.password.getProviderInfoForNewUser(
          password
        ),
    },
  };
  user = await createUser(newUser);
  // try {
  //   await sendNewEmailConfirmationCode(user.id, false);
  // } catch (e) {
  //   console.log("failed sending confirmation email on registration", e);
  // }
  return user;
}

export async function loginUserWithPassword(
  email: string,
  password: string
): Promise<User> {
  const user = await prisma.user.findUnique({
    where: {
      email,
    },
  });
  if (!user)
    throw new ServerError(
      ServerErrorCode.wrong_credentials,
      "couldn't find a user with that email"
    );
  if (!user.providers.includes(AuthProvider.PASSWORD))
    throw new ServerError(
      ServerErrorCode.wrong_credentials,
      "the user doesn't have a password"
    );
  await authenticationProviders.password.authenticate(
    (user.providerInfo as Prisma.JsonObject).password,
    { password }
  );
  return user;
}
