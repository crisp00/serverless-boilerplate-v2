import { UserGroup } from "@prisma/client";

export const UserPermission = {
  demo: "demo",
  confirmEmail: "confirmEmail",
  getProfile: "getProfile",
  patchProfile: "patchProfile",
  // plop handler permission hook
  devGenerateTestData: "devGenerateTestData",
  adminStats: "adminStats",
  inviteUser: "inviteUser",
  adminUserAutocompleteByEmail: "adminUserAutocompleteByEmail",
  adminDeleteFoodEntry: "adminDeleteFoodEntry",
  adminUpsertFoodEntry: "adminUpsertFoodEntry",
  adminGetFoodEntries: "adminGetFoodEntries",
  getFoodEntries: "getFoodEntries",
  deleteFoodEntry: "deleteFoodEntry",
  updateFoodEntry: "updateFoodEntry",
  updateMeal: "updateMeal",
  addFoodEntry: "addFoodEntry",
  updatePassword: "updatePassword",
  resetPassword: "resetPassword",
  requestPasswordResetEmail: "requestPasswordResetEmail",
};

export const userGroups = {
  ...UserGroup,
  GUEST: "GUEST",
  EMAIL_UNCONFIRMED: "EMAIL_UNCONFIRMED",
};

const groupPermissions: { [group: string]: string[] } = {
  [userGroups.GUEST]: [],
};

groupPermissions[userGroups.CUSTOMER] = [
  ...groupPermissions[userGroups.GUEST],
  UserPermission.getProfile,
  UserPermission.updatePassword,
  UserPermission.addFoodEntry,
  UserPermission.deleteFoodEntry,
  UserPermission.updateMeal,
  UserPermission.getFoodEntries,
  UserPermission.patchProfile,
  UserPermission.inviteUser,
];
groupPermissions[userGroups.ADMIN] = [
  ...groupPermissions[userGroups.CUSTOMER],
  UserPermission.adminGetFoodEntries,
  UserPermission.adminUpsertFoodEntry,
  UserPermission.adminDeleteFoodEntry,
  UserPermission.adminUserAutocompleteByEmail,
  UserPermission.adminStats,
  UserPermission.devGenerateTestData,
];

export { groupPermissions };
