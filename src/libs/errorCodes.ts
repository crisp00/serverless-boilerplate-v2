/* eslint-disable camelcase */
export enum ServerErrorCode {
  not_found,
  invalid_input,
  wrong_credentials,
  server_error,
  forbidden,
  unauthorized,
  token_expired,
  password_not_secure,
  not_implemented,
  not_enought_time_elapse_since_last_attempt,
  password_reset_code_expired,
  password_reset_wrong_code,
  max_entries_in_meal_for_day,
}

export const SERVER_ERROR_MAP = {
  [ServerErrorCode.not_found]: {
    statusCode: 404,
    description: "",
  },
  [ServerErrorCode.invalid_input]: {
    statusCode: 400,
    description: "",
  },
  [ServerErrorCode.server_error]: {
    statusCode: 500,
    description: "",
  },
  [ServerErrorCode.not_implemented]: {
    statusCode: 500,
    description: "This feature has not been implemented yet",
  },
  [ServerErrorCode.unauthorized]: {
    statusCode: 401,
    description: "",
  },
  [ServerErrorCode.forbidden]: {
    statusCode: 403,
    description: "You're not allowed to do that",
  },
  [ServerErrorCode.token_expired]: {
    statusCode: 401,
    description: "",
  },
  [ServerErrorCode.password_not_secure]: {
    statusCode: 400,
    description: "The provided password doesn't meet the security criteria",
  },
  [ServerErrorCode.not_enought_time_elapse_since_last_attempt]: {
    statusCode: 400,
    description: "You can't do that again after so little time",
  },
  [ServerErrorCode.password_reset_code_expired]: {
    statusCode: 400,
    description: "The code has expired",
  },
  [ServerErrorCode.password_reset_wrong_code]: {
    statusCode: 400,
    description: "The code has expired",
  },
  [ServerErrorCode.wrong_credentials]: {
    statusCode: 400,
    description: "The provided credentials do no match.",
  },
  [ServerErrorCode.max_entries_in_meal_for_day]: {
    statusCode: 400,
    description:
      "The meal already has the maximum amount of entries for that day.",
  },
};
