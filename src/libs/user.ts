import { User, AuthProvider } from ".prisma/client";
import { ServerError } from "./error";
import { ServerErrorCode } from "./errorCodes";
import { getPrisma } from "./prisma";
import authenticationProviders from "./authenticationProviders";

const prisma = getPrisma();

export function updateProfile(
  userId: number,
  data: Partial<Pick<User, "dailyCalorieLimit">>
): Promise<User> {
  return prisma.user.update({
    where: {
      id: userId,
    },
    data: {
      ...data,
    },
  });
}

export async function updatePassword(oldPassword, newPassword, userId) {
  if (
    !newPassword.match(
      /^(?=.{8,16}$)(?=.*\d)(?=.*[!"£$%&/()=?;:@#])(?=.*[a-zA-Z]).+$/
    )
  )
    throw new ServerError(
      ServerErrorCode.invalid_input,
      "The password doesn't meet the minimum complexity criteria"
    );
  const user = await prisma.user.findUnique({
    where: {
      id: userId,
    },
  });
  if (!user) throw new ServerError(ServerErrorCode.server_error);
  if (user.providers.includes(AuthProvider.PASSWORD)) {
    await authenticationProviders.password.authenticate(
      (user.providerInfo as any).password,
      { password: oldPassword }
    );
  }
  const providers = new Set(user.providers);
  providers.add(AuthProvider.PASSWORD);
  const password =
    await authenticationProviders.password.getProviderInfoForNewUser(
      newPassword
    );
  await prisma.user.update({
    where: {
      id: userId,
    },
    data: {
      providerInfo: {
        ...(user.providerInfo as any),
        password,
      },
    },
  });
}

export async function createUser(userData): Promise<User> {
  // eslint-disable-next-line no-shadow
  return prisma.$transaction(async (prisma) => {
    const user = await prisma.user.create({
      data: userData,
    });
    return user;
  });
}
