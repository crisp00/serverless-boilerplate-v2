import Joi from "joi";
import j2s from "joi-to-swagger";
import { isFunction } from "util";
import { HttpMethods } from "./constants";
import { ServerErrorCode, SERVER_ERROR_MAP } from "./errorCodes";
import { ServerlessConfig, serverlessConfig } from "./serverless";

export function swaggerRequestBody({ schema, description, required = true }) {
  return {
    description,
    required,
    content: {
      "application/json": {
        schema,
      },
    },
  };
}

export function definitions({
  name,
  summary,
  description,
  requestBodyDescription,
  responseBodyDescription,
  tags,
  inputValidator: inputValidator_,
  inputBody,
  outputValidator: outputValidator_,
  outputBody,
  path,
  filePath: filePath_,
  method,
  authenticated = false,
  requiredPermissions = [],
  errorResponses = [],
  warmup = false,
}: {
  name: string;
  summary?: string;
  description?: string;
  requestBodyDescription?: string;
  responseBodyDescription?: string;
  tags?: string[];
  inputValidator?: Joi.AnySchema;
  outputValidator?: Joi.AnySchema;
  inputBody?: Joi.AnySchema;
  outputBody?: Joi.AnySchema;
  path: string;
  filePath?: string;
  method: HttpMethods;
  authenticated?: boolean;
  requiredPermissions?: string[];
  errorResponses?: ServerErrorCode[];
  warmup?: boolean;
}): {
  inputValidator: Joi.AnySchema;
  outputValidator: Joi.AnySchema;
  serverless: () => ServerlessConfig;
  method: HttpMethods;
  path: string;
  swagger: () => any;
  requiredPermissions: string[];
  name: string;
  errorResponses: ServerErrorCode[];
} {
  let inputValidator: Joi.AnySchema;
  if (inputValidator_) {
    inputValidator = inputValidator_;
  } else {
    inputValidator = Joi.object()
      .keys(
        inputBody
          ? {
              body: inputBody,
            }
          : {}
      )
      .unknown(true);
  }
  let outputValidator: Joi.AnySchema;
  if (outputValidator_) {
    outputValidator = outputValidator_;
  } else {
    outputValidator = Joi.object()
      .keys(
        outputBody
          ? {
              statusCode: Joi.number(),
              body: outputBody,
            }
          : {
              statusCode: Joi.number(),
            }
      )
      .unknown(true);
  }
  const filePath = filePath_ || path;
  const serverless = serverlessConfig({
    path,
    filePath,
    method,
    authenticated,
    warmup,
  });
  const inputBodySwagger = inputBody ? j2s(inputBody) : null;
  const outputBodySwagger = outputBody ? j2s(outputBody) : null;
  return {
    name,
    method,
    path,
    inputValidator,
    outputValidator,
    errorResponses,
    requiredPermissions,
    serverless() {
      return serverless;
    },
    swagger() {
      let security;
      let parameters: any[] = [];
      const pathParameters = path.match(/\{\w+\}/g);
      if (pathParameters?.length) {
        pathParameters.forEach((match) => {
          const name = match.replace(/[\{\}]/g, "");
          parameters.push({
            in: "path",
            name,
            required: true,
          });
        });
      }
      const inputObj = inputValidator.describe();
      const queryStringParameters = inputObj.keys.queryStringParameters;
      if (queryStringParameters) {
        Object.keys(queryStringParameters.keys).forEach((key) => {
          const obj = queryStringParameters.keys[key];
          parameters.push({
            in: "query",
            name: key,
            required: obj.flags?.presence === "required",
          });
        });
      }
      console.log();
      if (authenticated) {
        security = [
          {
            bearerAuth: [],
          },
        ];
      }
      return [
        {
          operationId: name,
          tags,
          summary,
          description,
          security,
          parameters,
          requestBody: inputBodySwagger
            ? swaggerRequestBody({
                schema: inputBodySwagger.swagger,
                description: requestBodyDescription,
              })
            : undefined,
          responses: {
            "200": {
              description: responseBodyDescription,
              content: outputBodySwagger
                ? {
                    "application/json": {
                      schema: outputBodySwagger.swagger,
                    },
                  }
                : undefined,
            },
            ...errorResponses.reduce(
              (obj, curr) => ({
                ...obj,
                [SERVER_ERROR_MAP[curr].statusCode]: {
                  description: SERVER_ERROR_MAP[curr].description,
                },
              }),
              {}
            ),
          },
        },
        {
          ...(inputBodySwagger || { components: [] }).components,
          ...(outputBodySwagger || { components: [] }).components,
        },
      ];
    },
  };
}
