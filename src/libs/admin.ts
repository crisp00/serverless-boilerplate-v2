import { prisma } from "@prisma/client";
import moment from "moment-timezone";
import { getPrisma } from "./prisma";

export async function userAutocomplete(prisma = getPrisma(), search: string) {
  let userId, userIdValid;
  try {
    userId = parseInt(search, 10);
  } catch (e) {}
  userIdValid = typeof userId === "number" && userId >= 0;
  return await prisma.user.findMany({
    where: {
      OR: [
        ...(userIdValid ? [{ id: parseInt(search, 10) }] : []),
        { email: { contains: search, mode: "insensitive" } },
        { name: { contains: search, mode: "insensitive" } },
      ],
    },
    take: 100,
    select: {
      id: true,
      email: true,
      name: true,
    },
  });
}

export async function adminStats(prisma = getPrisma()) {
  const res = await prisma.$transaction([
    prisma.foodEntry.count({
      // number of ADDED entries in last 7 days (including today)
      where: {
        createdAt: {
          gte: moment().subtract(6, "days").startOf("day").toDate(),
        },
      },
    }),
    prisma.foodEntry.count({
      // number of ADDED entries in previous 7 days
      where: {
        createdAt: {
          gte: moment().subtract(13, "days").startOf("day").toDate(),
          lte: moment().subtract(7, "days").endOf("day").toDate(),
        },
      },
    }),
    prisma.foodEntry.aggregate({
      // calories ADDED in the last 7 days
      where: {
        createdAt: {
          gte: moment().subtract(6, "days").startOf("day").toDate(),
        },
      },
      _sum: {
        calories: true,
      },
    }),
    prisma.user.count({
      // count of users who have ADDED at least a foodEntry in the last 7 days
      where: {
        foodEntries: {
          some: {
            createdAt: {
              gte: moment().subtract(6, "days").startOf("day").toDate(),
            },
          },
        },
      },
    }),
    prisma.user.count({}), // count of users
    prisma.foodEntry.aggregate({
      // calories CONSUMED in the last 7 days
      where: {
        consumptionTime: {
          gte: moment().subtract(6, "days").startOf("day").toDate(),
        },
      },
      _sum: {
        calories: true,
      },
    }),
    prisma.user.count({
      // count of users who have CONSUMED at least a foodEntry in the last 7 days
      where: {
        foodEntries: {
          some: {
            consumptionTime: {
              gte: moment().subtract(6, "days").startOf("day").toDate(),
            },
          },
        },
      },
    }),
    prisma.foodEntry.count({
      // number of CONSUMED entries in last 7 days (including today)
      where: {
        consumptionTime: {
          gte: moment().subtract(6, "days").startOf("day").toDate(),
        },
      },
    }),
    prisma.foodEntry.count({
      // number of CONSUMED entries in previous 7 days
      where: {
        consumptionTime: {
          gte: moment().subtract(13, "days").startOf("day").toDate(),
          lte: moment().subtract(7, "days").endOf("day").toDate(),
        },
      },
    }),
    prisma.$queryRaw`
      SELECT COUNT(id) as count, DATE("consumptionTime") as day, SUM("calories") as consumedCalories 
      FROM "FoodEntry" fe WHERE "consumptionTime" >= ${moment()
        .tz("GMT")
        .subtract(6, "days")
        .startOf("day")
        .toDate()} AND "consumptionTime" < ${moment()
      .tz("GMT")
      .endOf("day")
      .toDate()} 
      GROUP BY day;`,
  ]);

  return {
    entriesAddedLast7Days: res[0],
    entriesAddedPrevious7Days: res[1],
    caloriesAddedLast7Days: res[2]._sum.calories,
    usersWhoAddedLast7Days: res[3],
    totalUsers: res[4],
    caloriesConsumedLast7Days: res[5]._sum.calories,
    usersWhoConsumedLast7Days: res[6],
    entriesConsumedLast7Days: res[7],
    entriesConsumedPrevious7Days: res[8],
    caloriesConsumptionGraph: res[9],
  };
}
