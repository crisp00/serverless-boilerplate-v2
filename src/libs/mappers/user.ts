import { User } from ".prisma/client";
import { UserJoi, UserResponse } from "../types/user";

export function mapUserForPublic(user: User): UserResponse {
  return {
    id: user.id,
    email: user.email,
    providers: user.providers,
    group: user.group,
    name: user.name,
    surname: user.surname,
    dailyCalorieLimit: user.dailyCalorieLimit,
  };
}
