export enum HttpMethods {
  get = "get",
  post = "post",
  patch = "patch",
  delete = "delete",
  put = "put",
}
