import bcrypt from "bcryptjs";
import { Prisma } from ".prisma/client";
import { AuthenticationProvider } from "./types";
import { ServerError } from "../error";
import { ServerErrorCode } from "../errorCodes";
import { getPrisma } from "../prisma";

const prisma = getPrisma();

const passwordProvider: AuthenticationProvider = {
  async authenticate(
    providerInfo: { passwordHash: string },
    authenticationInfo: { password: string }
  ) {
    const passwordsMatch = await bcrypt.compare(
      authenticationInfo.password,
      (providerInfo as Prisma.JsonObject).passwordHash
    );
    if (!passwordsMatch)
      throw new ServerError(
        ServerErrorCode.wrong_credentials,
        "the password doesn't match"
      );
    // TODO: finish this up
    return true;
  },
  async getProviderInfoForNewUser(password: string) {
    return {
      passwordHash: await bcrypt.hash(password, 8),
    };
  },
  requiresEmailConfirmation: false,
};

export default passwordProvider;
