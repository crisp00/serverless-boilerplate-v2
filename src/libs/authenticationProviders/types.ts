import { Prisma } from ".prisma/client";

export interface AuthenticationProvider {
  authenticate: (...any) => Promise<boolean>;
  getProviderInfoForNewUser: (...any) => Promise<Prisma.JsonValue>;
  requiresEmailConfirmation: boolean;
}
