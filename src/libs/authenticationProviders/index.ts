import { AuthProvider } from ".prisma/client";
import { ServerError } from "../error";
import { ServerErrorCode } from "../errorCodes";
import password from "./password";
import { AuthenticationProvider } from "./types";

export default {
  password,
};

export function getProvider(provider: AuthProvider): AuthenticationProvider {
  switch (provider) {
    case AuthProvider.PASSWORD:
      return password;
    default:
      throw new ServerError(
        ServerErrorCode.server_error,
        "unrecognized authentication provider"
      );
  }
}
