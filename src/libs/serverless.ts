import { HttpMethods } from "./constants";

type ServerlessConfigProps = {
  path: string;
  method: HttpMethods;
  authenticated?: boolean;
  filePath?: string;
  warmup?: boolean;
};

export type ServerlessConfig = {
  handler: string;
  warmup?: boolean;
  events: any[];
};

export function serverlessConfig({
  path,
  method = HttpMethods.get,
  authenticated = false,
  filePath,
  warmup = false,
}: ServerlessConfigProps): ServerlessConfig {
  return {
    handler: `src/handlers/${filePath || path}/handler.handler`,
    events: [
      {
        http: {
          method,
          path,
          authorizer: authenticated ? "authorize" : undefined,
          cors: true,
        },
      },
      ...(warmup
        ? [
            {
              schedule: {
                rate: "rate(5 minutes)",
                enabled: true,
                input: {
                  warmer: true,
                  concurrency: 1,
                },
              },
            },
          ]
        : []),
    ],
  };
}
