import Joi from "joi";
import { AuthProvider, User, UserGroup } from ".prisma/client";

export const UserJoi = Joi.object({
  id: Joi.number(),
  email: Joi.string().email(),
  providers: Joi.array().items(
    Joi.string().valid(...Object.keys(AuthProvider))
  ),
  group: Joi.valid(...Object.keys(UserGroup)),
  name: Joi.string().allow(null),
}).unknown();

export const TokensJoi = Joi.object({
  accessToken: Joi.string(),
  refreshToken: Joi.string(),
});

export type UserResponse = Pick<
  User,
  "id" | "email" | "name" | "providers" | "group" | "dailyCalorieLimit"
>;
export interface TokensResponse {
  accessToken: string;
  refreshToken: string;
}
