import handlers from "./src/handlers";

const ci = process.env.CI;

const functions = {};

handlers.forEach((handler) => {
  functions[handler.name] = handler.serverless;
});

const serverlessConfiguration: any = {
  service: {
    name: "backend",
  },
  frameworkVersion: "2",
  custom: {
    webpack: {
      webpackConfig: "./webpack.config.js",
      includeModules: {
        forceExclude: [".prisma"],
      },
      packagerOptions: {
        scripts: [
          'PRISMA_CLI_BINARY_TARGETS="rhel-openssl-1.0.x" npx prisma generate',
          "rm node_modules/.prisma/client/libquery_engine-darwin-* || true",
          "rm node_modules/.prisma/client/libquery_engine-debian-* || true",
          "rm node_modules/prisma/engines/*/libquery_engine-darwin-* || true",
          "rm node_modules/prisma/engines/*/libquery_engine-debian-* || true",
          "rm node_modules/prisma/libquery_engine-darwin-* || true",
          "rm node_modules/prisma/libquery_engine-debian-* || true",
          "rm -rf node_modules/@prisma/engines/**",
        ],
      },
    },
    stage: "${opt:stage, self:provider.stage}",
    environments: {
      dev: "${file(./ci/dev.config.json)}",
      staging: "${file(./ci/staging.config.json)}",
      prod: "${file(./ci/prod.config.json)}",
    },
  },
  useDotenv: true,
  plugins: [
    "serverless-webpack",
    "serverless-offline",
    "serverless-iam-roles-per-function",
    "serverless-prune-plugin",
    "serverless-aws-documentation",
  ],
  provider: {
    name: "aws",
    runtime: "nodejs12.x",
    region: "eu-central-1",
    timeout: 60,
    stage: "dev",
    iamRoleStatements: [
      {
        Effect: "Allow",
        Action: ["lambda:InvokeFunction", "lambda:InvokeAsync"],
        Resource: "*",
      },
    ],
    profile: !ci ? "pintea" : undefined,
    architecture: "x86_64",
    versionFunctions: false,
    apiGateway: {
      minimumCompressionSize: 1024,
    },
    environment: "${self:custom.environments.${self:custom.stage}}",
  },
  functions,
  resources: {
    Resources: {
      GatewayResponse: {
        Type: "AWS::ApiGateway::GatewayResponse",
        Properties: {
          ResponseParameters: {
            "gatewayresponse.header.Access-Control-Allow-Origin": "'*'",
            "gatewayresponse.header.Access-Control-Allow-Headers": "'*'",
          },
          ResponseType: "EXPIRED_TOKEN",
          RestApiId: {
            Ref: "ApiGatewayRestApi",
          },
          StatusCode: "401",
        },
      },
      AuthFailureGatewayResponse: {
        Type: "AWS::ApiGateway::GatewayResponse",
        Properties: {
          ResponseParameters: {
            "gatewayresponse.header.Access-Control-Allow-Origin": "'*'",
            "gatewayresponse.header.Access-Control-Allow-Headers": "'*'",
          },
          ResponseType: "UNAUTHORIZED",
          RestApiId: {
            Ref: "ApiGatewayRestApi",
          },
          StatusCode: "401",
        },
      },
    },
  },
};

module.exports = serverlessConfiguration;
